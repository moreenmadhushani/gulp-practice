var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync").create();
function style() {
    return (
        gulp
            .src("app/scss/*.scss")
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(gulp.dest("app/css"))
            .pipe(browserSync.stream())
    );
}
exports.browserSync = browserSync

function reload() {
    browserSync.reload();
}
exports.reload = reload

function watch() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });
    gulp.watch('app/scss/*.scss', style)
    gulp.watch('app/*.html', reload).on('change', browserSync.reload);
}

exports.watch = watch